<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Clients;


class ClientsController extends Controller
{
      public function listar()
      {
          $clientes = Clients::all();
          $data=[
              'code'=>200,
              'clientes'=>$clientes];
          return response() -> json($clientes);
      }
      
      public function crear(Request $request){
        if (!empty($request -> all())) {
            $validate = Validator::make($request -> all(), [
            'nombre' => 'required',
            'apellido' => 'required',
            'telefono' => 'required',
            'mail' => 'required',
            'fecha_nacimiento' => 'required'
            
            ]);
            
            if ($validate ->fails()) {
                $data = [
                    'code' => 400,
                    'status' => 'error',
                    'mensaje' => 'No ha ingresado todos los parametros',
                    'errores' => $validate -> errors()
                ];
            } else{
                $clients = Clients::where('nombre', $request ->nombre)->first();
                if (empty($clients)) {
                    $clients = new Clients();
                    $clients ->secretaria_id =$request->secretaria_id;
                    $clients ->nombre =$request->nombre;
                    $clients ->apellido = $request->apellido;
                    $clients ->telefono = $request->telefono;
                    $clients ->mail = $request->mail;
                    $clients ->fecha_nacimiento = $request -> fecha_nacimiento; 
                    $clients ->save();
                    
                    $data =[
                        'code' =>200,
                        'status' => 'success',
                        'mensaje' => 'Se ha creado correctamente',
                        'cliente' => $clients
                    ];
                }else{
                    $data = [
                        'code' => 400,
                        'status' => 'error',
                        'mensaje' => 'Ya existe el cliente'
                    ];

                }
            }
        } else {
            $data = [
                'code' =>400,
                'status' => 'error',
                'mensaje' => 'Error al crear la cliente'
            ];
        }
        return response() ->json($data);
    }

   public function editar(Request $request)
    {
        if (!empty($request -> all())) {
            $validate = Validator::make($request ->all(), [
                'nombre' => 'required',
                'apellido' => 'required',
                'telefono' => 'required',
                'mail' => 'required',
                'fecha_nacimiento' => 'required'
                
            ]);
            if ($validate -> fails()) {
                $data = [
                    'code' => 400,
                    'status' => 'error',
                    'mensaje' => 'No ha ingresado todos los parametros',
                    'errores' => $validate -> errors()
                ];
            } else {
                $cliente = Clients::find($request ->id);
                if ($cliente !=null) {
                    $cliente ->nombre = $request -> nombre;
                    $cliente ->apellido = $request -> apellido;
                    $cliente ->telefono = $request -> telefono;
                    $cliente ->mail = $request -> mail;
                    $cliente ->fecha_nacimiento = $request -> fecha_nacimiento;
                    $cliente ->save();
                    $data = [
                    'code' => 200,
                    'status' => 'success',
                    'mensaje' => 'Se ha editado correctamente',
                    'Cliente' => $cliente
                ];
                } else {
                    $data = [
                        'code' => 400,
                        'status' => 'error',
                        'mensaje' => 'No se encuentra el cliente'];
                }
            }
        } else {
            $data = [
                'code' => 400,
                'status' => 'error',
                'mensaje' => 'Error al editar'];
        }

        return response()->json($data);
    }

    public function eliminar(Request $request)
    {
        if ($request ->id == '') {
            $data = [
                'code' => 400,
                'status' => 'error',
                'mensaje' => 'Debe ingresar un ID del cliente para eliminar'
            ];
            return response() -> json($data);
        }
        $cliente = Clients::find($request ->id);
        if ($cliente != null) {
            $cliente -> delete();
            $data = [
                'code' => 200,
                'status' => 'success',
                'mensaje' => 'Se ha eliminado correctamente'
            ];
        } else {
            $data = [
                    'code' => 200,
                    'status' => 'success',
                    'mensaje' => 'No se encontro el ID'
                ];
        }
        return response() -> json($data);
    }
    




}
