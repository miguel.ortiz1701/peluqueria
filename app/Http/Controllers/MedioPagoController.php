<?php

namespace App\Http\Controllers;
use App\Models\Medio_Pago;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class MedioPagoController extends Controller
{
    public function listar()
    {
        $mediopago = Medio_Pago::all();
        $data=[
            'code'=>200,
            'mediopago'=>$mediopago];
        return response() -> json($mediopago);
    }
    public function crear(Request $request){
        if (!empty($request -> all())) {
            $validate = Validator::make($request -> all(), [
            'monto' => 'required',
            'forma_pago' => 'required',
            ]);
            if ($validate ->fails()) {
                $data = [
                    'code' => 400,
                    'status' => 'error',
                    'mensaje' => 'No ha ingresado todos los parametros',
                    'errores' => $validate -> errors()
                ];
            } else{
                $mediopago = Medio_Pago::where('forma_pago', $request ->forma_pago)->first();
                if (empty($mediopago)) {
                    $mediopago = new Medio_Pago();
                    $mediopago ->monto =$request->monto;
                    $mediopago ->forma_pago = $request->forma_pago;
                   
                    $mediopago ->save();

                    $data =[
                        'code' =>200,
                        'status' => 'success',
                        'mensaje' => 'Se ha creado correctamente',
                        'mediopago$mediopago' => $mediopago
                    ];
                }else{
                    $data = [
                        'code' => 400,
                        'status' => 'error',
                        'mensaje' => 'Ya existe el medio de pago'
                    ];

                }
            }
        } else {
            $data = [
                'code' =>400,
                'status' => 'error',
                'mensaje' => 'Error al crear el Medio de pago'
            ];
        }
        return response() ->json($data);
    }

}
