<?php

namespace App\Http\Controllers;


use App\Models\Peluquera;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PeluqueraController extends Controller
{
    public function listarPeluqueraPorOrdenAlfabetico(){
        $peluquera = DB::table('peluquera') -> select ('*') -> orderBy('apellido') -> get();
        $data = [
                'code' => 200,
                'peluquera' => $peluquera

        ];
        return response() -> json($data);
    }
}

