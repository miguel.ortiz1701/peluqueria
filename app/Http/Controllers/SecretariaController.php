<?php

namespace App\Http\Controllers;

use App\Models\Secretaria;
use App\Models\Ventas;
use  Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SecretariaController extends Controller
{
    
        public function registrarVenta(Request $request){
            if (!empty($request -> all())) {
                $validate = Validator::make($request -> all(), [
                'nombre' => 'required',
                'apellido' => 'required',
                'telefono' => 'required',
                'mail' => 'required',
                'fecha_nacimiento' => 'required',
                
                ]);
                if ($validate ->fails()) {
                    $data = [
                        'code' => 400,
                        'status' => 'error',
                        'mensaje' => 'No ha ingresado todos los parametros',
                        'errores' => $validate -> errors()
                    ];
                } else{
                    $secretaria = Secretaria::where('nombre', $request ->nombre)->first();
                    if (empty($secretaria)) {
    
                        $secretaria = new Secretaria();
                        $secretaria ->nombre =$request->nombre;
                        $secretaria ->apellido = $request->apellido;
                        $secretaria ->telefono = $request->telefono;
                        $secretaria ->mail = $request->mail;
                        $secretaria ->fecha_nacimiento = $request -> fecha_nacimiento;
                        
                        $secretaria ->save();
    
                        $data =[
                            'code' =>200,
                            'status' => 'success',
                            'mensaje' => 'Se ha creado correctamente',
                            'secretaria' => $secretaria
                        ];
                    }else{
                        $data = [
                            'code' => 400,
                            'status' => 'error',
                            'mensaje' => 'Ya existe la venta'
                        ];
    
                    }
                }
            } else {
                $data = [
                    'code' =>400,
                    'status' => 'error',
                    'mensaje' => 'Error al crear la venta'
                ];
            }
            return response() ->json($data);
        }
    }

