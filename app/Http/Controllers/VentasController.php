<?php

namespace App\Http\Controllers;

use App\Models\Ventas;
use  Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class VentasController extends Controller
{
    public function listar()
    {
        $ventas = Ventas::all();
        $data=[
            'code'=>200,
            'ventas'=>$ventas];
        return response() -> json($ventas);
    }

    public function crear(Request $request){
        if (!empty($request -> all())) {
            $validate = Validator::make($request -> all(), [
            'medio_pago' => 'required',
            'nro_documento' => 'required',
            
            ]);
            if ($validate ->fails()) {
                $data = [
                    'code' => 400,
                    'status' => 'error',
                    'mensaje' => 'No ha ingresado todos los parametros',
                    'errores' => $validate -> errors()
                ];
            } else{
                $ventas = Ventas::where('nro_documento', $request ->nro_documento)->first();
                if (empty($ventas)) {

                    $ventas = new Ventas();
                    $ventas ->medio_pago =$request->medio_pago;
                    $ventas ->nro_documento = $request->nro_documento;
                    $ventas ->secretaria_id = $request->secretaria_id;
                    $ventas ->fecha_venta = date('Y-m-d');
                    
                    $ventas ->save();

                    $data =[
                        'code' =>200,
                        'status' => 'success',
                        'mensaje' => 'Se ha creado correctamente',
                        'ventas' => $ventas
                    ];
                }else{
                    $data = [
                        'code' => 400,
                        'status' => 'error',
                        'mensaje' => 'Ya existe la venta'
                    ];

                }
            }
        } else {
            $data = [
                'code' =>400,
                'status' => 'error',
                'mensaje' => 'Error al crear la venta'
            ];
        }
        return response() ->json($data);
    }

    public function editar(Request $request)
    {
        if (!empty($request -> all())) {
            $validate = Validator::make($request -> all(), [
                'secretaria_id' => 'required',
                'modio_pago' => 'required',
                'nro_documento' => 'required',
                'fecha_venta' => 'required',
            ]);
            if ($validate -> fails()) {
                $data = [
                    'code' => 400,
                    'status' => 'error',
                    'mensaje' => 'Error al editar la venta',
                    'errores' => $validate -> errors()
                ];
            }else{
                $venta = Ventas::find($request -> id);
                if ($venta != null) {
                    $venta -> secretaria_id = $request -> secretaria_id;
                    $venta -> medio_pago = $request -> medio_pago;
                    $venta -> nro_documento = $request -> nro_documento; 
                    $venta -> fecha_venta = $request -> fecha_venta;
                    $venta -> save();
                    $data = [
                        'code' => 200,
                        'status' => 'success',
                        'mensaje' => 'Se ha editado correctamente la venta',
                        'Venta' => $venta
                    ];
                }else{
                    $data = [
                        'code' => 400,
                        'status' => 'error',
                        'mensaje' => 'No se encuentra la venta'
                    ];
                }
            }
        }else{
            $data = [
                'code' => 400,
                'status' => 'error',
                'mensaje' => 'No se han ingresado los datos de la venta correctamente'
            ];
        }
        return response() -> json($data);
    }

    public function eliminar (Request $request){
        if ($request->id_ventas == ''){
            $data = [
                'code' =>400,
                'status' => 'error',
                'mensaje' => 'Debe ingresar un ID de una venta'
            ];
        }else{
            $ventas = Ventas::findOrfail($request->id_ventas);
            if(empty($ventas)){
                $data = [
                    'code' =>400,
                    'status' => 'error',
                    'mensaje' => 'No se encontro la venta asociada al ID'
                ];
            }else{
                $ventas ->delete();
                $data = [
                    'code' =>200,
                    'status' => 'success',
                    'mensaje' => 'Se ha eliminado correctamente'
                ];
            }
        }
        return response() -> json ($data);
    }

    public function VentaPorIdSecretaria(Request $request)
    {
        if(!empty($request->all())){
            $validate = Validator::make($request->all(),[
                'secretaria_id' => 'required'
            ]);
            if($validate ->fails()){
                $data = [
                    'code' => 400,
                    'status' => 'error',
                    'mensaje' => 'No ha ingresado un id correcto',
                    'errores' => $validate -> errors()
                ];
            }else{
                $ventas = Ventas::where('secretaria_id', $request -> secretaria_id)->get();
                if(empty($ventas)){
                    $data = [
                        'code' => 400,
                        'status' => 'error',
                        'mensaje' => 'No existen ventas asociada a ese id'
                    ];
                }else{
                    $data = [
                        'code' => 200,
                        'status' => 'success',
                        'ventas' => $ventas
                    ];
                }
            }
        }
        return response() -> json($data);
    }
    
}
