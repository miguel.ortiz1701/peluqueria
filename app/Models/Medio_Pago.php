<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Medio_Pago extends Model
{
    protected $table = 'medio_pago';
    public $timestamps = false;
}
