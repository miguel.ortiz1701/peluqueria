<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Peluquera extends Model{
    protected $table = "peluquera";
    public $timestamps = false;
}

?>
