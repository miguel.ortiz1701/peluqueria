<?php

use App\Http\Controllers\ClientsController;
use App\Http\Controllers\MedioPagoController;
use App\Http\Controllers\PeluqueraController;
use App\Http\Controllers\ReservasController;
use App\Http\Controllers\SecretariaController;
use App\Http\Controllers\VentasController;
use App\Models\MedioPago;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//----------------------------------------------------------------
//ruta de cliente

Route::get('/clients/listar', [ClientsController::class, 'listar']);
Route::post('/clients/crear', [ClientsController::class,'crear']);
Route::put('/clients/editar',[ClientsController::class, 'editar']);
Route::post('/clients/eliminar', [ClientsController::class,'eliminar']);

//ruta de venta

Route::get('/ventas/listar',[VentasController::class, 'listar']);
Route::post('/ventas/crear', [VentasController::class, 'crear']);
Route::put('/ventas/editar', [VentasController::class, 'editar']);
Route::post('/ventas/eliminar', [VentasController::class, 'eliminar']);
Route::post('/ventas/VentaPorIdSecretaria', [VentasController::class, 'VentaPorIdSecretaria']);

//ruta de secretaria


//ruta peluquera

Route::get('/peluquera/listarPeluqueraPorOrdenAlfabetico', [PeluqueraController::class, 'listarPeluqueraPorOrdenAlfabetico']);

//ruta reserva

Route::get('/reservas/aprobarReserva', [ReservasController::class, 'aprobarReserva']);

//ruta medio_pago

Route::get('/medio_pago/listar', [MedioPagoController::class, 'listar']);